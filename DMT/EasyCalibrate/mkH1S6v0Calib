#! /bin/sh -x
#
#  THis script makes the reference calibration file for SenseMon
#
#  The frequency response function and open loop gain are from the 
#  calibration cvs repository in:
#  calibration/frequencydomain/runs/S6/H1/model/V0
#
#  Use the olg sweep by Jeff Kissel at GPS ~928831104 for the DarmErr 
#  CalLine amplitude (2.72e-06).
#  The calibration line constants are from the appropriate "A1" channels
#  in the LineMon_H1_exc trend frames
#
cat > E14v00-header.xml <<EOF
<?xml version="1.0"?>
<!DOCTYPE LIGO_LW SYSTEM "http://ldas-sw.ligo.caltech.edu/doc/ligolwAPI/html/ligolw_dtd.txt">
<LIGO_LW>
  <LIGO_LW>
    <Param Name="Channel">H1:LSC-DARM_ERR</Param>
    <Param Name="Comment">H1 DARM_ERR calibration S6 V0</Param>
    <Time Name="StartTime" Type="GPS">929904671</Time>
    <Param Name="Duration" Type="double">0</Param>
    <Param Name="EXCChannel">H1:LSC-DARM_CTRL_EXC_DAQ</Param>
    <Param Name="CalLineFreq" Type="double">1144.3</Param>
    <Param Name="CalLineAmplASQ" Type="double">2.72e-06</Param>
    <Param Name="CalLineAmplEXC" Type="double">0.16</Param>
    <Table Name="DARMChannels">
      <Column Name="ChannelName" Type="string"/>
      <Column Name="RefValue" Type="float"/>
      <Stream Type="Local" Delimiter=" ">
        "H1:LSC-DARM_GAIN" -6.3
      </Stream>
    </Table>
  </LIGO_LW>
</LIGO_LW>
EOF
mkcalibfile -c H1:LSC-DARM_ERR -x E14v00-header.xml -fmax 4000 \
	-olg H-H1_CAL_REF_OLOOP_GAIN_S6_V0-929904671-95328.txt \
	-cav H-H1_CAL_REF_CAV_GAIN_DARM_ERR_SP_S6_V0-929904671-95328.txt \
	-o ReferenceCalibrationDarmErr_H1.xml
#
#  Using this file with DARM_GAIN set to either -6.3 or -3.0 doesn't work
#	-resp H-H1_CAL_REF_RESPONSE_DARM_ERR_S6_V0-928270000-1246415.txt

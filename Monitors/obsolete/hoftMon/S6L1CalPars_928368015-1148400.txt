#
#  L1 h(t) calibration parameters from Xavi 7/6/2009
#  These parameters match the filter file in the calibration cvs at:
#  calibration/timedomain/runs/S6/L1/V0/S6L1Filters_928368015-1148400.txt
#  Note that this filter file replaces one of the same name.
#
 --olg-re 0.121720272649524 --olg-im 0.138542534236204
 --whitener-re 0.00998472832896893 --whitener-im -0.000552770881673091
 --cal-line-freq 1151.5

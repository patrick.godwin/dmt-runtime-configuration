###########################################################
# Parameters to control the Noise Floor Monitor
# Roberto Grosso
# 18.04.2005
#
# Sintax: PARAMETER_NAME = VALUE
# Note: the blanks are important, following examples will
#       not work:
#         PARAMETER_NAME =VALUE
#         PARAMTER_NAME= VALUE
###########################################################

###########################################################
# Detector Channel.
DETECTOR_CHANNEL = H2:LSC-DARM_ERR

###########################################################
# Order of the Notch Filter
NOTCH_FILTER_ORDER = 4096

###########################################################
# time stride to update whitening filter, e.g. every 10th
# time stride.
UPDATE_WHITENING_FILTER = 10

###########################################################
# Order of the Whitening Filter.
ORDER_WHITENING_FILTER = 512

###########################################################
# Number of frequencies in the PSD used in the whitening
# filter.
FREQUENCIES_PSD = 2048

###########################################################
# Size in sec. of the time series used for the
# computation of the pds used in the whitening filter.
SIZE_PSD = 8

###########################################################
# Size is Hz. of the running median filter for
# the computation of the Whitening filter.
RM_FILTER_FOR_WHITENING_FILTER = 50

###########################################################
# Size in sec. of the Running median filter.
SIZE_RM_FILTER = 4

###########################################################
# Filter order for cutoff low-pass filter
FLT_ORDER = 16384

###########################################################
# Cuttof frequency in Hz.
CUTOFF_FREQ = 2048

###########################################################
# The signal is split into four bands called band A, B, C
# and D. The frequency corners of the bands are in Hz.
#
#  Band A: 0 (zero)    - BAND_FREQ_A
#  Band B: BAND_FREQ_B - BAND_FREQ_A
#  Band C: BAND_FREQ_C - BAND_FREQ_B
#  Band D: CUTOFF_FREQ - BAND_FREQ_C
#
###########################################################
# Filter order for sinal bands processing.
BAND_FLT_ORDER = 4096

###########################################################
# Frequency A in Hz. for band selection.
BAND_FREQ_A = 20

###########################################################
# Frequency B in Hz. for band selection.
BAND_FREQ_B = 100

###########################################################
# Frequency C in Hz. for band selection.
BAND_FREQ_C = 200

###########################################################
# Time Stride in sec.
TIME_STRIDE = 60

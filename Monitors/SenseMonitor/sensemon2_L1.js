{
   "channel" : "L1:GDS-CALB_STRAIN",
   "osc-file" : "hoft_dq_flags.osc",
   "osc-break-cond" : "hoft_Up",
   "osc-gate-cond" : "",
   "trend-file" : "",
   "resample" : 8192.0,
   "highpass" : 8.0,
   "highpass-dt" : 2.0,
   "prep-filter" : "gain(1)",
   "f-low" : 10.0,
   "f-high" : 2000.0,
   "stride" : 60,
   "H0_Mpc" : 73.8,
   "ranges" : [
      {"monitor-channel" : "L1:SNSH-RANGE_MPC",
       "trend-channel"   : "L1:SNSH-RANGE_MPC",
       "gw-type"         :  "bns-1.4x1.4",
       "threshold-sigma" :  10.0,
       "psd-estimate"    : "welch", // or "median-mean"
       "window"          : "hanning",
       "window-parameter": "0",
       "fd-calibration"  : "zpk()",
       "red-shift"       : true,
       "red-monitor"     : "L1:SNSH-RANGE_Z",
       "red-trend"       : "L1:SNSH-RANGE_Z",
       "psd-monitor"     : "L1:SNSH-RANGE_PSD",
       "weight-monitor"  : "L1:SNSH-RANGE_WEIGHT"
      },
      {"monitor-channel" : "L1:SNSB-RANGE_MPC",
       "trend-channel"   : "L1:SNSB-RANGE_MPC",
       "gw-type"         :  "bns-30x30",
       "threshold-sigma" :  10.0,
       "psd-estimate"    : "welch", // or "median-mean"
       "window"          : "hanning",
       "window-parameter": "0.00",
       "fd-calibration"  : "zpk()",
       "red-shift"       : true,
       "red-monitor"     : "L1:SNSB-RANGE_Z",
       "red-trend"       : "L1:SNSB-RANGE_Z",
       "psd-monitor"     : "L1:SNSB-RANGE_PSD",
       "weight-monitor"  : "L1:SNSB-RANGE_WEIGHT"
      }
   ]
}

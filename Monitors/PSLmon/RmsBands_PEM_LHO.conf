#
#  RMS bands
#
#  The RmsBands process calculates, trends and displays the RMS power
#  in specific frequency bands of predominately PEM channels.
#
#  2010.10.20 jgz PEM channels only.
#
#--> Parameters
Parameter MonName RmsBand
Parameter ChanPfx RMSB
Parameter Stride 5.0
Parameter TrigEnable 1
Parameter OSCFile /export/home/ops/pars/LockLoss.conf
#
#--> Window definition(s)
Filter Hann Hanning
#
#--> Channels
Channel H0:PEM-BSC10_ACC1Y -window Hann -overlap 5.0
Channel H0:PEM-BSC10_MAGY  -window Hann -overlap 5.0
Channel H0:PEM-BSC10_MIC   -window Hann -overlap 5.0
Channel H0:PEM-BSC1_MAG1Y  -window Hann -overlap 5.0
Channel H0:PEM-BSC4_ACCX   -window Hann -overlap 5.0
Channel H0:PEM-BSC4_ACCY   -window Hann -overlap 5.0
Channel H0:PEM-BSC5_MIC    -window Hann -overlap 5.0
Channel H0:PEM-BSC6_MIC    -window Hann -overlap 5.0
Channel H0:PEM-BSC7_ACCX   -window Hann -overlap 5.0
Channel H0:PEM-BSC7_ACCY   -window Hann -overlap 5.0
Channel H0:PEM-BSC8_ACCX   -window Hann -overlap 5.0
Channel H0:PEM-BSC8_ACCY   -window Hann -overlap 5.0
Channel H0:PEM-BSC9_ACC1X  -window Hann -overlap 5.0
Channel H0:PEM-BSC9_MAGX   -window Hann -overlap 5.0
Channel H0:PEM-BSC9_MIC    -window Hann -overlap 5.0
Channel H0:PEM-COIL_MAGZ   -window Hann -overlap 5.0
Channel H0:PEM-EX_SEISX    -window Hann -overlap 5.0
Channel H0:PEM-EY_SEISY    -window Hann -overlap 5.0
Channel H0:PEM-HAM1_MIC    -window Hann -overlap 5.0
Channel H0:PEM-HAM7_ACCX   -window Hann -overlap 5.0
Channel H0:PEM-HAM8_ACCX   -window Hann -overlap 5.0
Channel H0:PEM-HAM9_ACCX   -window Hann -overlap 5.0
Channel H0:PEM-ISCT1_MIC   -window Hann -overlap 5.0
Channel H0:PEM-ISCT10_MIC  -window Hann -overlap 5.0
Channel H0:PEM-ISCT4_MIC   -window Hann -overlap 5.0
Channel H0:PEM-ISCT7_MIC   -window Hann -overlap 5.0
Channel H0:PEM-LVEA_MIC    -window Hann -overlap 5.0
Channel H0:PEM-LVEA_SEISX  -window Hann -overlap 5.0
Channel H0:PEM-LVEA_SEISY  -window Hann -overlap 5.0
Channel H0:PEM-MX_SEISX    -window Hann -overlap 5.0
Channel H0:PEM-MY_SEISY    -window Hann -overlap 5.0
Channel H0:PEM-PSL1_MIC    -window Hann -overlap 5.0
Channel H0:PEM-PSL2_ACCX   -window Hann -overlap 5.0
Channel H0:PEM-PSL2_MIC    -window Hann -overlap 5.0
Channel H0:PEM-VAULT_SEISX -window Hann -overlap 5.0
Channel H0:PEM-VAULT_SEISY -window Hann -overlap 5.0

Band  a1 H0:PEM-BSC10_ACC1Y -fmin   1 -fmax   4
Band  b1 H0:PEM-BSC10_ACC1Y -fmin   4 -fmax  12
Band  c1 H0:PEM-BSC10_ACC1Y -fmin  12 -fmax  17
Band  d1 H0:PEM-BSC10_ACC1Y -fmin  17 -fmax  40
Band  e1 H0:PEM-BSC10_ACC1Y -fmin  40 -fmax  57
Band  f1 H0:PEM-BSC10_ACC1Y -fmin  57 -fmax  60
Band  g1 H0:PEM-BSC10_ACC1Y -fmin  60 -fmax  62
Band  h1 H0:PEM-BSC10_ACC1Y -fmin  62 -fmax 100
Band  i1 H0:PEM-BSC10_ACC1Y -fmin 100 -fmax 400
Band  j1 H0:PEM-BSC10_ACC1Y -fmin 400 -fmax 900
Band  k1 H0:PEM-BSC10_ACC1Y -fmin 380 -fmax 400

Band  a2 H0:PEM-BSC10_MAGY -fmin   1 -fmax   4
Band  b2 H0:PEM-BSC10_MAGY -fmin   4 -fmax  12
Band  c2 H0:PEM-BSC10_MAGY -fmin  12 -fmax  17
Band  d2 H0:PEM-BSC10_MAGY -fmin  17 -fmax  40
Band  e2 H0:PEM-BSC10_MAGY -fmin  40 -fmax  57
Band  f2 H0:PEM-BSC10_MAGY -fmin  57 -fmax  60
Band  g2 H0:PEM-BSC10_MAGY -fmin  60 -fmax  62
Band  h2 H0:PEM-BSC10_MAGY -fmin  62 -fmax 100
Band  i2 H0:PEM-BSC10_MAGY -fmin 100 -fmax 400
Band  j2 H0:PEM-BSC10_MAGY -fmin 400 -fmax 900
Band  k2 H0:PEM-BSC10_MAGY -fmin 380 -fmax 400
 
Band  a3 H0:PEM-BSC10_MIC  -fmin   1 -fmax   4
Band  b3 H0:PEM-BSC10_MIC  -fmin   4 -fmax  12
Band  c3 H0:PEM-BSC10_MIC  -fmin  12 -fmax  17
Band  d3 H0:PEM-BSC10_MIC  -fmin  17 -fmax  40
Band  e3 H0:PEM-BSC10_MIC  -fmin  40 -fmax  57
Band  f3 H0:PEM-BSC10_MIC  -fmin  57 -fmax  60
Band  g3 H0:PEM-BSC10_MIC  -fmin  60 -fmax  62
Band  h3 H0:PEM-BSC10_MIC  -fmin  62 -fmax 100
Band  i3 H0:PEM-BSC10_MIC  -fmin 100 -fmax 400
Band  j3 H0:PEM-BSC10_MIC  -fmin 400 -fmax 900
Band  k3 H0:PEM-BSC10_MIC  -fmin 380 -fmax 400
 
Band  a4 H0:PEM-BSC1_MAG1Y -fmin   1 -fmax   4
Band  b4 H0:PEM-BSC1_MAG1Y -fmin   4 -fmax  12
Band  c4 H0:PEM-BSC1_MAG1Y -fmin  12 -fmax  17
Band  d4 H0:PEM-BSC1_MAG1Y -fmin  17 -fmax  40
Band  e4 H0:PEM-BSC1_MAG1Y -fmin  40 -fmax  57
Band  f4 H0:PEM-BSC1_MAG1Y -fmin  57 -fmax  60
Band  g4 H0:PEM-BSC1_MAG1Y -fmin  60 -fmax  62
Band  h4 H0:PEM-BSC1_MAG1Y -fmin  62 -fmax 100
Band  i4 H0:PEM-BSC1_MAG1Y -fmin 100 -fmax 400
Band  j4 H0:PEM-BSC1_MAG1Y -fmin 400 -fmax 900
Band  k4 H0:PEM-BSC1_MAG1Y -fmin 380 -fmax 400
 
Band  a5 H0:PEM-BSC4_ACCX  -fmin   1 -fmax   4
Band  b5 H0:PEM-BSC4_ACCX  -fmin   4 -fmax  12
Band  c5 H0:PEM-BSC4_ACCX  -fmin  12 -fmax  17
Band  d5 H0:PEM-BSC4_ACCX  -fmin  17 -fmax  40
Band  e5 H0:PEM-BSC4_ACCX  -fmin  40 -fmax  57
Band  f5 H0:PEM-BSC4_ACCX  -fmin  57 -fmax  60
Band  g5 H0:PEM-BSC4_ACCX  -fmin  60 -fmax  62
Band  h5 H0:PEM-BSC4_ACCX  -fmin  62 -fmax 100
Band  i5 H0:PEM-BSC4_ACCX  -fmin 100 -fmax 400
Band  j5 H0:PEM-BSC4_ACCX  -fmin 400 -fmax 900
Band  k5 H0:PEM-BSC4_ACCX  -fmin 380 -fmax 400
 
Band  a6 H0:PEM-BSC4_ACCY  -fmin   1 -fmax   4
Band  b6 H0:PEM-BSC4_ACCY  -fmin   4 -fmax  12
Band  c6 H0:PEM-BSC4_ACCY  -fmin  12 -fmax  17
Band  d6 H0:PEM-BSC4_ACCY  -fmin  17 -fmax  40
Band  e6 H0:PEM-BSC4_ACCY  -fmin  40 -fmax  57
Band  f6 H0:PEM-BSC4_ACCY  -fmin  57 -fmax  60
Band  g6 H0:PEM-BSC4_ACCY  -fmin  60 -fmax  62
Band  h6 H0:PEM-BSC4_ACCY  -fmin  62 -fmax 100
Band  i6 H0:PEM-BSC4_ACCY  -fmin 100 -fmax 400
Band  j6 H0:PEM-BSC4_ACCY  -fmin 400 -fmax 900
Band  k6 H0:PEM-BSC4_ACCY  -fmin 380 -fmax 400
 
Band  a8 H0:PEM-BSC5_MIC   -fmin   1 -fmax   4
Band  b8 H0:PEM-BSC5_MIC   -fmin   4 -fmax  12
Band  c8 H0:PEM-BSC5_MIC   -fmin  12 -fmax  17
Band  d8 H0:PEM-BSC5_MIC   -fmin  17 -fmax  40
Band  e8 H0:PEM-BSC5_MIC   -fmin  40 -fmax  57
Band  f8 H0:PEM-BSC5_MIC   -fmin  57 -fmax  60
Band  g8 H0:PEM-BSC5_MIC   -fmin  60 -fmax  62
Band  h8 H0:PEM-BSC5_MIC   -fmin  62 -fmax 100
Band  i8 H0:PEM-BSC5_MIC   -fmin 100 -fmax 400
Band  j8 H0:PEM-BSC5_MIC   -fmin 400 -fmax 900
Band  k8 H0:PEM-BSC5_MIC   -fmin 380 -fmax 400
 
Band  a10 H0:PEM-BSC6_MIC  -fmin   1 -fmax   4
Band  b10 H0:PEM-BSC6_MIC  -fmin   4 -fmax  12
Band  c10 H0:PEM-BSC6_MIC  -fmin  12 -fmax  17
Band  d10 H0:PEM-BSC6_MIC  -fmin  17 -fmax  40
Band  e10 H0:PEM-BSC6_MIC  -fmin  40 -fmax  57
Band  f10 H0:PEM-BSC6_MIC  -fmin  57 -fmax  60
Band  g10 H0:PEM-BSC6_MIC  -fmin  60 -fmax  62
Band  h10 H0:PEM-BSC6_MIC  -fmin  62 -fmax 100
Band  i10 H0:PEM-BSC6_MIC  -fmin 100 -fmax 400
Band  j10 H0:PEM-BSC6_MIC  -fmin 400 -fmax 900
Band  k10 H0:PEM-BSC6_MIC  -fmin 380 -fmax 400
 
Band  a11 H0:PEM-BSC7_ACCX -fmin   1 -fmax   4
Band  b11 H0:PEM-BSC7_ACCX -fmin   4 -fmax  12
Band  c11 H0:PEM-BSC7_ACCX -fmin  12 -fmax  17
Band  d11 H0:PEM-BSC7_ACCX -fmin  17 -fmax  40
Band  e11 H0:PEM-BSC7_ACCX -fmin  40 -fmax  57
Band  f11 H0:PEM-BSC7_ACCX -fmin  57 -fmax  60
Band  g11 H0:PEM-BSC7_ACCX -fmin  60 -fmax  62
Band  h11 H0:PEM-BSC7_ACCX -fmin  62 -fmax 100
Band  i11 H0:PEM-BSC7_ACCX -fmin 100 -fmax 400
Band  j11 H0:PEM-BSC7_ACCX -fmin 400 -fmax 900
Band  k11 H0:PEM-BSC7_ACCX -fmin 380 -fmax 400
 
Band  a12 H0:PEM-BSC7_ACCY -fmin   1 -fmax   4
Band  b12 H0:PEM-BSC7_ACCY -fmin   4 -fmax  12
Band  c12 H0:PEM-BSC7_ACCY -fmin  12 -fmax  17
Band  d12 H0:PEM-BSC7_ACCY -fmin  17 -fmax  40
Band  e12 H0:PEM-BSC7_ACCY -fmin  40 -fmax  57
Band  f12 H0:PEM-BSC7_ACCY -fmin  57 -fmax  60
Band  g12 H0:PEM-BSC7_ACCY -fmin  60 -fmax  62
Band  h12 H0:PEM-BSC7_ACCY -fmin  62 -fmax 100
Band  i12 H0:PEM-BSC7_ACCY -fmin 100 -fmax 400
Band  j12 H0:PEM-BSC7_ACCY -fmin 400 -fmax 900
Band  k12 H0:PEM-BSC7_ACCY -fmin 380 -fmax 400
 
Band  a13 H0:PEM-BSC8_ACCX -fmin   1 -fmax   4
Band  b13 H0:PEM-BSC8_ACCX -fmin   4 -fmax  12
Band  c13 H0:PEM-BSC8_ACCX -fmin  12 -fmax  17
Band  d13 H0:PEM-BSC8_ACCX -fmin  17 -fmax  40
Band  e13 H0:PEM-BSC8_ACCX -fmin  40 -fmax  57
Band  f13 H0:PEM-BSC8_ACCX -fmin  57 -fmax  60
Band  g13 H0:PEM-BSC8_ACCX -fmin  60 -fmax  62
Band  h13 H0:PEM-BSC8_ACCX -fmin  62 -fmax 100
Band  i13 H0:PEM-BSC8_ACCX -fmin 100 -fmax 400
Band  j13 H0:PEM-BSC8_ACCX -fmin 400 -fmax 900
Band  k13 H0:PEM-BSC8_ACCX -fmin 380 -fmax 400
 
Band  a14 H0:PEM-BSC8_ACCY -fmin   1 -fmax   4
Band  b14 H0:PEM-BSC8_ACCY -fmin   4 -fmax  12
Band  c14 H0:PEM-BSC8_ACCY -fmin  12 -fmax  17
Band  d14 H0:PEM-BSC8_ACCY -fmin  17 -fmax  40
Band  e14 H0:PEM-BSC8_ACCY -fmin  40 -fmax  57
Band  f14 H0:PEM-BSC8_ACCY -fmin  57 -fmax  60
Band  g14 H0:PEM-BSC8_ACCY -fmin  60 -fmax  62
Band  h14 H0:PEM-BSC8_ACCY -fmin  62 -fmax 100
Band  i14 H0:PEM-BSC8_ACCY -fmin 100 -fmax 400
Band  j14 H0:PEM-BSC8_ACCY -fmin 400 -fmax 900
Band  k14 H0:PEM-BSC8_ACCY -fmin 380 -fmax 400
 
Band  a15 H0:PEM-BSC9_ACC1X -fmin   1 -fmax   4
Band  b15 H0:PEM-BSC9_ACC1X -fmin   4 -fmax  12
Band  c15 H0:PEM-BSC9_ACC1X -fmin  12 -fmax  17
Band  d15 H0:PEM-BSC9_ACC1X -fmin  17 -fmax  40
Band  e15 H0:PEM-BSC9_ACC1X -fmin  40 -fmax  57
Band  f15 H0:PEM-BSC9_ACC1X -fmin  57 -fmax  60
Band  g15 H0:PEM-BSC9_ACC1X -fmin  60 -fmax  62
Band  h15 H0:PEM-BSC9_ACC1X -fmin  62 -fmax 100
Band  i15 H0:PEM-BSC9_ACC1X -fmin 100 -fmax 400
Band  j15 H0:PEM-BSC9_ACC1X -fmin 400 -fmax 900
Band  k15 H0:PEM-BSC9_ACC1X -fmin 380 -fmax 400
 
Band  a16 H0:PEM-BSC9_MAGX  -fmin   1 -fmax   4
Band  b16 H0:PEM-BSC9_MAGX  -fmin   4 -fmax  12
Band  c16 H0:PEM-BSC9_MAGX  -fmin  12 -fmax  17
Band  d16 H0:PEM-BSC9_MAGX  -fmin  17 -fmax  40
Band  e16 H0:PEM-BSC9_MAGX  -fmin  40 -fmax  57
Band  f16 H0:PEM-BSC9_MAGX  -fmin  57 -fmax  60
Band  g16 H0:PEM-BSC9_MAGX  -fmin  60 -fmax  62
Band  h16 H0:PEM-BSC9_MAGX  -fmin  62 -fmax 100
Band  i16 H0:PEM-BSC9_MAGX  -fmin 100 -fmax 400
Band  j16 H0:PEM-BSC9_MAGX  -fmin 400 -fmax 900
Band  k16 H0:PEM-BSC9_MAGX  -fmin 380 -fmax 400
 
Band  a17 H0:PEM-BSC9_MIC   -fmin   1 -fmax   4
Band  b17 H0:PEM-BSC9_MIC   -fmin   4 -fmax  12
Band  c17 H0:PEM-BSC9_MIC   -fmin  12 -fmax  17
Band  d17 H0:PEM-BSC9_MIC   -fmin  17 -fmax  40
Band  e17 H0:PEM-BSC9_MIC   -fmin  40 -fmax  57
Band  f17 H0:PEM-BSC9_MIC   -fmin  57 -fmax  60
Band  g17 H0:PEM-BSC9_MIC   -fmin  60 -fmax  62
Band  h17 H0:PEM-BSC9_MIC   -fmin  62 -fmax 100
Band  i17 H0:PEM-BSC9_MIC   -fmin 100 -fmax 400
Band  j17 H0:PEM-BSC9_MIC   -fmin 400 -fmax 900
Band  k17 H0:PEM-BSC9_MIC   -fmin 380 -fmax 400
 
Band  a18 H0:PEM-COIL_MAGZ  -fmin   1 -fmax   4
Band  b18 H0:PEM-COIL_MAGZ  -fmin   4 -fmax  12
Band  c18 H0:PEM-COIL_MAGZ  -fmin  12 -fmax  17
Band  d18 H0:PEM-COIL_MAGZ  -fmin  17 -fmax  40
Band  e18 H0:PEM-COIL_MAGZ  -fmin  40 -fmax  57
Band  f18 H0:PEM-COIL_MAGZ  -fmin  57 -fmax  60
Band  g18 H0:PEM-COIL_MAGZ  -fmin  60 -fmax  62
Band  h18 H0:PEM-COIL_MAGZ  -fmin  62 -fmax 100
Band  i18 H0:PEM-COIL_MAGZ  -fmin 100 -fmax 400
Band  j18 H0:PEM-COIL_MAGZ  -fmin 400 -fmax 900
Band  k18 H0:PEM-COIL_MAGZ  -fmin 380 -fmax 400
 
Band  a19 H0:PEM-EX_SEISX   -fmin   1 -fmax   4
Band  b19 H0:PEM-EX_SEISX   -fmin   4 -fmax  12
Band  c19 H0:PEM-EX_SEISX   -fmin  12 -fmax  17
Band  d19 H0:PEM-EX_SEISX   -fmin  17 -fmax  40
Band  e19 H0:PEM-EX_SEISX   -fmin  40 -fmax  57
Band  f19 H0:PEM-EX_SEISX   -fmin  57 -fmax  60
Band  g19 H0:PEM-EX_SEISX   -fmin  60 -fmax  62
Band  h19 H0:PEM-EX_SEISX   -fmin  62 -fmax 100
 
Band  a21 H0:PEM-EY_SEISY   -fmin   1 -fmax   4
Band  b21 H0:PEM-EY_SEISY   -fmin   4 -fmax  12
Band  c21 H0:PEM-EY_SEISY   -fmin  12 -fmax  17
Band  d21 H0:PEM-EY_SEISY   -fmin  17 -fmax  40
Band  e21 H0:PEM-EY_SEISY   -fmin  40 -fmax  57
Band  f21 H0:PEM-EY_SEISY   -fmin  57 -fmax  60
Band  g21 H0:PEM-EY_SEISY   -fmin  60 -fmax  62
Band  h21 H0:PEM-EY_SEISY   -fmin  62 -fmax 100
 
Band  a23 H0:PEM-HAM7_ACCX  -fmin   1 -fmax   4
Band  b23 H0:PEM-HAM7_ACCX  -fmin   4 -fmax  12
Band  c23 H0:PEM-HAM7_ACCX  -fmin  12 -fmax  17
Band  d23 H0:PEM-HAM7_ACCX  -fmin  17 -fmax  40
Band  e23 H0:PEM-HAM7_ACCX  -fmin  40 -fmax  57
Band  f23 H0:PEM-HAM7_ACCX  -fmin  57 -fmax  60
Band  g23 H0:PEM-HAM7_ACCX  -fmin  60 -fmax  62
Band  h23 H0:PEM-HAM7_ACCX  -fmin  62 -fmax 100
Band  i23 H0:PEM-HAM7_ACCX  -fmin 100 -fmax 400
Band  j23 H0:PEM-HAM7_ACCX  -fmin 400 -fmax 900
Band  k23 H0:PEM-HAM7_ACCX  -fmin 380 -fmax 400
 
Band  a24 H0:PEM-HAM8_ACCX  -fmin   1 -fmax   4
Band  b24 H0:PEM-HAM8_ACCX  -fmin   4 -fmax  12
Band  c24 H0:PEM-HAM8_ACCX  -fmin  12 -fmax  17
Band  d24 H0:PEM-HAM8_ACCX  -fmin  17 -fmax  40
Band  e24 H0:PEM-HAM8_ACCX  -fmin  40 -fmax  57
Band  f24 H0:PEM-HAM8_ACCX  -fmin  57 -fmax  60
Band  g24 H0:PEM-HAM8_ACCX  -fmin  60 -fmax  62
Band  h24 H0:PEM-HAM8_ACCX  -fmin  62 -fmax 100
Band  i24 H0:PEM-HAM8_ACCX  -fmin 100 -fmax 400
Band  j24 H0:PEM-HAM8_ACCX  -fmin 400 -fmax 900
Band  k24 H0:PEM-HAM8_ACCX  -fmin 380 -fmax 400
 
Band  a25 H0:PEM-HAM9_ACCX  -fmin   1 -fmax   4
Band  b25 H0:PEM-HAM9_ACCX  -fmin   4 -fmax  12
Band  c25 H0:PEM-HAM9_ACCX  -fmin  12 -fmax  17
Band  d25 H0:PEM-HAM9_ACCX  -fmin  17 -fmax  40
Band  e25 H0:PEM-HAM9_ACCX  -fmin  40 -fmax  57
Band  f25 H0:PEM-HAM9_ACCX  -fmin  57 -fmax  60
Band  g25 H0:PEM-HAM9_ACCX  -fmin  60 -fmax  62
Band  h25 H0:PEM-HAM9_ACCX  -fmin  62 -fmax 100
Band  i25 H0:PEM-HAM9_ACCX  -fmin 100 -fmax 400
Band  j25 H0:PEM-HAM9_ACCX  -fmin 400 -fmax 900
Band  k25 H0:PEM-HAM9_ACCX  -fmin 380 -fmax 400
 
Band  a28 H0:PEM-LVEA_SEISX -fmin   1 -fmax   4
Band  b28 H0:PEM-LVEA_SEISX -fmin   4 -fmax  12
Band  c28 H0:PEM-LVEA_SEISX -fmin  12 -fmax  17
Band  d28 H0:PEM-LVEA_SEISX -fmin  17 -fmax  40
Band  e28 H0:PEM-LVEA_SEISX -fmin  40 -fmax  57
Band  f28 H0:PEM-LVEA_SEISX -fmin  57 -fmax  60
Band  g28 H0:PEM-LVEA_SEISX -fmin  60 -fmax  62
Band  h28 H0:PEM-LVEA_SEISX -fmin  62 -fmax 100
 
Band  a29 H0:PEM-LVEA_SEISY -fmin   1 -fmax   4
Band  b29 H0:PEM-LVEA_SEISY -fmin   4 -fmax  12
Band  c29 H0:PEM-LVEA_SEISY -fmin  12 -fmax  17
Band  d29 H0:PEM-LVEA_SEISY -fmin  17 -fmax  40
Band  e29 H0:PEM-LVEA_SEISY -fmin  40 -fmax  57
Band  f29 H0:PEM-LVEA_SEISY -fmin  57 -fmax  60
Band  g29 H0:PEM-LVEA_SEISY -fmin  60 -fmax  62
Band  h29 H0:PEM-LVEA_SEISY -fmin  62 -fmax 100
 
Band  a30 H0:PEM-MX_SEISX   -fmin   1 -fmax   4
Band  b30 H0:PEM-MX_SEISX   -fmin   4 -fmax  12
Band  c30 H0:PEM-MX_SEISX   -fmin  12 -fmax  17
Band  d30 H0:PEM-MX_SEISX   -fmin  17 -fmax  40
Band  e30 H0:PEM-MX_SEISX   -fmin  40 -fmax  57
Band  f30 H0:PEM-MX_SEISX   -fmin  57 -fmax  60
Band  g30 H0:PEM-MX_SEISX   -fmin  60 -fmax  62
Band  h30 H0:PEM-MX_SEISX   -fmin  62 -fmax 100
 
Band  a32 H0:PEM-MY_SEISY   -fmin   1 -fmax   4
Band  b32 H0:PEM-MY_SEISY   -fmin   4 -fmax  12
Band  c32 H0:PEM-MY_SEISY   -fmin  12 -fmax  17
Band  d32 H0:PEM-MY_SEISY   -fmin  17 -fmax  40
Band  e32 H0:PEM-MY_SEISY   -fmin  40 -fmax  57
Band  f32 H0:PEM-MY_SEISY   -fmin  57 -fmax  60
Band  g32 H0:PEM-MY_SEISY   -fmin  60 -fmax  62
Band  h32 H0:PEM-MY_SEISY   -fmin  62 -fmax 100
 
Band  a34 H0:PEM-PSL2_ACCX  -fmin   1 -fmax   4
Band  b34 H0:PEM-PSL2_ACCX  -fmin   4 -fmax  12
Band  c34 H0:PEM-PSL2_ACCX  -fmin  12 -fmax  17
Band  d34 H0:PEM-PSL2_ACCX  -fmin  17 -fmax  40
Band  e34 H0:PEM-PSL2_ACCX  -fmin  40 -fmax  57
Band  f34 H0:PEM-PSL2_ACCX  -fmin  57 -fmax  60
Band  g34 H0:PEM-PSL2_ACCX  -fmin  60 -fmax  62
Band  h34 H0:PEM-PSL2_ACCX  -fmin  62 -fmax 100
Band  i34 H0:PEM-PSL2_ACCX  -fmin 100 -fmax 400
Band  j34 H0:PEM-PSL2_ACCX  -fmin 400 -fmax 900
Band  k34 H0:PEM-PSL2_ACCX  -fmin 380 -fmax 400
 
Band  a35 H0:PEM-PSL2_MIC   -fmin   1 -fmax   4
Band  b35 H0:PEM-PSL2_MIC   -fmin   4 -fmax  12
Band  c35 H0:PEM-PSL2_MIC   -fmin  12 -fmax  17
Band  d35 H0:PEM-PSL2_MIC   -fmin  17 -fmax  40
Band  e35 H0:PEM-PSL2_MIC   -fmin  40 -fmax  57
Band  f35 H0:PEM-PSL2_MIC   -fmin  57 -fmax  60
Band  g35 H0:PEM-PSL2_MIC   -fmin  60 -fmax  62
Band  h35 H0:PEM-PSL2_MIC   -fmin  62 -fmax 100
Band  i35 H0:PEM-PSL2_MIC   -fmin 100 -fmax 400
Band  j35 H0:PEM-PSL2_MIC   -fmin 400 -fmax 900
Band  k35 H0:PEM-PSL2_MIC   -fmin 380 -fmax 400

Band  a51 H0:PEM-VAULT_SEISX -fmin   1 -fmax   4
Band  b51 H0:PEM-VAULT_SEISX -fmin   4 -fmax  12
Band  c51 H0:PEM-VAULT_SEISX -fmin  12 -fmax  17
Band  d51 H0:PEM-VAULT_SEISX -fmin  17 -fmax  40
Band  e51 H0:PEM-VAULT_SEISX -fmin  40 -fmax  57
Band  f51 H0:PEM-VAULT_SEISX -fmin  57 -fmax  60
Band  g51 H0:PEM-VAULT_SEISX -fmin  60 -fmax  62
Band  h51 H0:PEM-VAULT_SEISX -fmin  62 -fmax 100
 
Band  a52 H0:PEM-VAULT_SEISY -fmin   1 -fmax   4
Band  b52 H0:PEM-VAULT_SEISY -fmin   4 -fmax  12
Band  c52 H0:PEM-VAULT_SEISY -fmin  12 -fmax  17
Band  d52 H0:PEM-VAULT_SEISY -fmin  17 -fmax  40
Band  e52 H0:PEM-VAULT_SEISY -fmin  40 -fmax  57
Band  f52 H0:PEM-VAULT_SEISY -fmin  57 -fmax  60
Band  g52 H0:PEM-VAULT_SEISY -fmin  60 -fmax  62
Band  h52 H0:PEM-VAULT_SEISY -fmin  62 -fmax 100
#
#  Listen for noise in the airplane band
#
Band AirLvea   H0:PEM-LVEA_MIC   -fmin 62 -fmax 118 -tavg 300.0 \
				-fraclow 0 -frachi 1.5 -hist
Band AirBsc5   H0:PEM-BSC5_MIC   -fmin 62 -fmax 118 -tavg 300.0 \
	 			-fraclow 0 -frachi 1.5 -hist
Band AirBsc6   H0:PEM-BSC6_MIC   -fmin 62 -fmax 118 -tavg 300.0 \
	 			-fraclow 0 -frachi 1.5 -hist
Band AirBsc9   H0:PEM-BSC9_MIC   -fmin 62 -fmax 118 -tavg 300.0 \
				-fraclow 0 -frachi 1.5 -hist
Band AirBsc10  H0:PEM-BSC10_MIC  -fmin 62 -fmax 118 -tavg 300.0 \
				-fraclow 0 -frachi 1.5 -hist
Band AirHam1   H0:PEM-HAM1_MIC   -fmin 62 -fmax 118 -tavg 300.0 \
				-fraclow 0 -frachi 1.5 -hist
Band AirIsct1  H0:PEM-ISCT1_MIC  -fmin 62 -fmax 118 -tavg 300.0 \
				-fraclow 0 -frachi 1.5 -hist
Band AirIsct4  H0:PEM-ISCT4_MIC  -fmin 62 -fmax 118 -tavg 300.0 \
				-fraclow 0 -frachi 1.5 -hist
Band AirIsct10 H0:PEM-ISCT10_MIC -fmin 62 -fmax 118 -tavg 300.0 \
				-fraclow 0 -frachi 1.5 -hist
Band AirIsct7  H0:PEM-ISCT7_MIC  -fmin 62 -fmax 118 -tavg 300.0 \
				-fraclow 0 -frachi 1.5 -hist
Band AirPsl1   H0:PEM-PSL1_MIC   -fmin 62 -fmax 118 -tavg 300.0 \
				-fraclow 0 -frachi 1.5 -hist
Band AirPsl2   H0:PEM-PSL2_MIC   -fmin 62 -fmax 118 -tavg 300.0 \
				-fraclow 0 -frachi 1.5 -hist
#
# Look for seismic activity near the suspension resonances
Band SeisSmX H0:PEM-LVEA_SEISX	-fmin 0.9 -fmax 1.1 -tavg 1200 \
				-fraclow 0 -frachi 5.0 -hist
Band SeisSmY H0:PEM-LVEA_SEISY	-fmin 0.9 -fmax 1.1 -tavg 1200 \
				-fraclow 0 -frachi 5.0 -hist
Band SeisCoX H0:PEM-EX_SEISX	-fmin 0.4 -fmax 0.9 -tavg 1200 \
				-fraclow 0 -frachi 3.0 -hist
Band SeisBsY H0:PEM-EY_SEISY	-fmin 1.8 -fmax 2.4 -tavg 1200 \
				-fraclow 0 -frachi 3.0 -hist
